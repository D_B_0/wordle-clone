mod words;

use std::{
    fmt::Result,
    io::{self, Write},
};

use colored::Colorize;

pub fn get_random_word() -> &'static str {
    words::WORDS[rand::random::<usize>() % words::WORDS.len()]
}

fn main() -> Result {
    let word = get_random_word();
    // println!("Selected word: {}", word);
    let mut curr_guess = 0;
    let mut line = String::from("     ");
    loop {
        print!(
            "{}",
            if line.len() == 5 {
                "Your guess: "
            } else {
                "Please enter a 5 letter word: "
            }
        );
        io::stdout()
            .flush()
            .expect("Error printing to standard output");
        line.clear();
        io::stdin()
            .read_line(&mut line)
            .expect("Error reading input");
        // println!("Successfully read input: {:?}", line);
        line = line.trim().to_owned();
        let guess = line.to_owned();
        print!("\u{001b}[F");
        if guess.len() != 5 {
            print!("\r\u{001b}[0J");
            continue;
        }
        curr_guess += 1;
        let mut matches = 0;
        for (i, ch) in guess.char_indices() {
            if word.chars().nth(i).unwrap() == ch {
                matches += 1;
                print!("{}", "██ ".green());
            } else if word.chars().any(|c| c == ch) {
                print!("{}", "██ ".yellow());
            } else {
                print!("{}", "██ ".black());
            }
        }
        print!("\u{001b}[0J\n\n");
        if matches == 5 {
            println!("You guessed right!");
            break;
        }
        if curr_guess >= 5 {
            println!("You lost! The word was \"{}\"", word);
            break;
        }
    }
    Ok(())
}
